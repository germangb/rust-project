use {Error, Result};

use sdl2::event::Event;
use sdl2::EventPump;

use sdl2::keyboard::{KeyboardState, Keycode};

use sdl2::mouse::{MouseState, RelativeMouseState};

use std::slice::Iter;

pub struct Input {
    pump: EventPump,
    events: Vec<Event>,
}

impl Input {
    pub fn new(pump: EventPump) -> Self {
        Input {
            pump,
            events: Vec::new(),
        }
    }

    pub fn event_pump(&self) -> &EventPump {
        &self.pump
    }

    pub fn events(&self) -> Iter<Event> {
        self.events.iter()
    }

    pub fn keyboard_state(&self) -> KeyboardState {
        self.pump.keyboard_state()
    }

    pub fn mouse_state(&self) -> MouseState {
        self.pump.mouse_state()
    }

    pub fn relative_mouse_state(&self) -> RelativeMouseState {
        self.pump.relative_mouse_state()
    }
}

pub struct System;

impl System {
    pub fn update(input: &mut Input) -> Result<()> {
        input.events.clear();

        for event in input.pump.poll_iter() {
            match event {
                Event::Quit { .. } => return Err(Error::AlreadyClosed),
                _ => input.events.push(event),
            }
        }

        Ok(())
    }
}
