#[derive(Debug)]
pub struct Terrain {
    height: f32,
    size: usize,
}

impl Terrain {
    pub fn new(height: f32, size: usize) -> Self {
        Terrain { height, size }
    }

    pub fn height(&self) -> f32 {
        self.height
    }

    pub fn size(&self) -> usize {
        self.size
    }
}
