use {Error, Result};

use sdl2::TimerSubsystem;

pub struct Timer {
    timer: TimerSubsystem,
    elapsed: u32,

    // tick counter (computes FPS)
    ticks: u32,
    fps: u32,
    last_fps_update: u32,

    last_time: u32,
    delta: f32,
}

impl Timer {
    pub fn new(system: TimerSubsystem) -> Self {
        Self {
            timer: system,
            elapsed: 0,
            ticks: 0,
            fps: 0,
            last_fps_update: 0,

            last_time: 0,
            delta: 0.0,
        }
    }

    pub fn delta_time(&self) -> f32 {
        self.delta
    }

    pub fn elapsed(&self) -> f32 {
        self.elapsed as f32 / 1000.0
    }

    pub fn fps(&self) -> u32 {
        self.fps
    }
}

pub struct System;

impl System {
    pub fn update(timer: &mut Timer) -> Result<()> {
        timer.elapsed = timer.timer.ticks();

        timer.delta = (timer.elapsed - timer.last_time) as f32 / 1000.0;

        if timer.elapsed - timer.last_fps_update > 1000 {
            timer.last_fps_update = timer.elapsed;
            timer.fps = timer.ticks;
            timer.ticks = 0;
        } else {
            timer.ticks += 1;
        }

        timer.last_time = timer.elapsed;
        Ok(())
    }
}
