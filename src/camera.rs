use cgmath::prelude::*;
use cgmath::{Euler, Matrix4, PerspectiveFov, Rad, Vector3, Vector4};

#[derive(Debug, Copy, Clone)]
pub enum Rotation {
    SetEuler(Euler<Rad<f32>>),
    AddEuler(Euler<Rad<f32>>),
}

#[derive(Debug, Copy, Clone)]
pub enum Position {
    Add(Vector3<f32>),
    Set(Vector3<f32>),
}

#[derive(Debug)]
pub enum Projection {
    Perspective(PerspectiveFov<f32>),
}

#[derive(Debug)]
pub struct Camera {
    euler: Euler<Rad<f32>>,
    position: Vector3<f32>,
    projection: Projection,
    projection_matrix: Matrix4<f32>,
    view: Matrix4<f32>,
    view_projection: Matrix4<f32>,
    up: Vector3<f32>,
    right: Vector3<f32>,
}

impl Camera {
    /// Create a new camera with a given projection
    pub fn new(perspective: PerspectiveFov<f32>) -> Self {
        Camera {
            projection: Projection::Perspective(perspective.clone()),
            euler: Euler {
                x: Rad(0.0),
                y: Rad(0.0),
                z: Rad(0.0),
            },
            position: Vector3::zero(),

            projection_matrix: perspective.into(),
            view: Matrix4::identity(),
            view_projection: Matrix4::identity(),

            up: Vector3::zero(),
            right: Vector3::zero(),
        }
    }

    /// Updates the position and rotation state of the camera.
    pub fn update_view(&mut self, rotation: Option<Rotation>, position: Option<Position>) {
        match position {
            Some(Position::Add(v)) => self.position += v,
            Some(Position::Set(v)) => self.position = v,
            _ => {}
        }

        match rotation {
            Some(Rotation::SetEuler(e)) => self.euler = e,
            Some(Rotation::AddEuler(e)) => {
                self.euler.x += e.x;
                self.euler.y += e.y;
                self.euler.z += e.z;
            }
            _ => {}
        }
    }

    /// Updates the internally stores transforms of the camera:
    pub fn commit(&mut self) {
        let view: Matrix4<f32> = self.euler.into();
        self.view = view * Matrix4::from_translation(-self.position);

        self.view_projection = self.projection_matrix * self.view;

        if let Some(inv) = self.view_projection.invert() {
            self.up = (inv * Vector4::new(0.0, 1.0, 0.0, 0.0))
                .truncate()
                .normalize();
            self.right = (inv * Vector4::new(1.0, 0.0, 0.0, 0.0))
                .truncate()
                .normalize();
        }
    }

    pub fn right(&self) -> Vector3<f32> {
        self.right
    }

    pub fn up(&self) -> Vector3<f32> {
        self.up
    }

    pub fn view(&self) -> Matrix4<f32> {
        self.view
    }

    pub fn projection(&self) -> Matrix4<f32> {
        self.projection_matrix
    }

    pub fn view_projection(&self) -> Matrix4<f32> {
        self.view_projection
    }
}
