use {Error, Result};

use fs::File;

use toml::from_str;

use std::collections::{BTreeMap, HashMap};
use std::io::Read;
use std::path::{Path, PathBuf};

use gltf_crate as gltf;

#[derive(Debug)]
pub struct Gltf {
    gltf: HashMap<String, (PathBuf, gltf::Gltf)>,
}

#[derive(Debug, Deserialize)]
struct GltfToml {
    path: String,
}

type GltfsToml = BTreeMap<String, GltfToml>;

impl Gltf {
    pub fn open<P: AsRef<Path>>(path: P) -> Result<Self> {
        let mut file = File::open(path.as_ref())?;

        let mut contents = String::new();
        file.read_to_string(&mut contents)?;

        let gltf_defs: GltfsToml =
            from_str(contents.as_str()).map_err(|_| Error::Other("Error opening gltf toml file"))?;

        let mut gltf = HashMap::new();
        for (name, toml) in gltf_defs {
            let path: PathBuf = toml.path.clone().into();
            let parsed = gltf::Gltf::from_reader(File::open(&path)?)
                .map_err(|_| Error::Other("Error opening gltf file"))?;

            gltf.insert(name, (path, parsed));
        }

        Ok(Gltf { gltf })
    }

    pub fn get<S: AsRef<str>>(&self, name: S) -> Option<(&PathBuf, &gltf::Gltf)> {
        self.gltf.get(name.as_ref()).map(|&(ref p, ref g)| (p, g))
    }
}
