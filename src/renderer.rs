use {Error, Result};

use std::collections::HashMap;

use gl;
use gl::types::*;

use sdl2::video::{GLContext, GLProfile, SwapInterval, Window};
use sdl2::VideoSubsystem;

use cgmath::prelude::*;
use cgmath::{Matrix4, Ortho, Point3, Vector3};

use cgmath_culling::{BoundingBox, FrustumCuller, Intersection};

use camera::Camera;
use debug::Debug;
use decals::Decals;
use framebuffers::{Framebuffer, Framebuffers};
use input::Input;
use lighting::Lighting;
use meshes::{Mesh, Meshes};
use models::Models;
use shaders::{Shader, ShaderResult, Shaders};
use terrain::Terrain;
use textures::{Texture, Textures};
use time::Timer;

use std::mem;

pub struct Renderer {
    video: VideoSubsystem,
    window: Window,
    width: usize,
    height: usize,
    context: GLContext,
    instancing: Instancing,
}

pub trait Instance {}

impl Instance for Matrix4<f32> {}
impl Instance for [f32; 16] {}

pub struct Instancing {
    handle: GLuint,
}

impl Drop for Instancing {
    fn drop(&mut self) {
        unsafe { gl::DeleteBuffers(1, &self.handle) }
    }
}

impl Instancing {
    fn new() -> Self {
        unsafe {
            let tmp = vec![0u8; 1024 * 1024];
            let mut vbo = 0;

            check!(gl::GenBuffers(1, &mut vbo));
            check!(gl::BindBuffer(gl::ARRAY_BUFFER, vbo));
            check!(gl::BufferData(
                gl::ARRAY_BUFFER,
                tmp.len() as _,
                tmp.as_ptr() as _,
                gl::STREAM_DRAW
            ));

            check!(gl::BindBuffer(gl::ARRAY_BUFFER, 0));
            Instancing { handle: vbo }
        }
    }

    pub fn handle(&self) -> GLuint {
        self.handle
    }
}

pub struct DrawParams<'a, U> {
    pub clear_color: Option<(f32, f32, f32, f32)>,
    pub clear_buffers: Option<GLenum>,
    pub viewport: (isize, isize, usize, usize),
    pub mesh: &'a Mesh,
    pub framebuffer: Option<(&'a Framebuffer, &'a [GLenum])>,
    pub textures: &'a [&'a Texture],
    pub program: &'a Shader,
    pub uniform_data: U,
    pub uniforms: Option<Box<Fn(&Shader, U)>>,
    pub depth_test: bool,
    pub instances: Option<usize>,
    pub wireframe: bool,
    pub backface_culling: bool,
    pub depth_mask: bool,
    pub alpha_blending: bool,
    pub scissor: Option<(i32, i32, usize, usize)>,
}

impl Renderer {
    pub fn new(video: VideoSubsystem, width: usize, height: usize) -> Renderer {
        {
            let gl_attr = video.gl_attr();
            gl_attr.set_context_profile(GLProfile::Core);
            gl_attr.set_context_version(4, 5);
        }

        let window = video
            .window("Window", width as _, height as _)
            .opengl()
            .position_centered()
            .build()
            .unwrap();

        gl::load_with(|s| video.gl_get_proc_address(s) as _);

        let context = window.gl_create_context().unwrap();

        video.gl_set_swap_interval(SwapInterval::VSync);

        Renderer {
            video,
            window,
            width,
            height,
            context,
            instancing: Instancing::new(),
        }
    }

    pub fn width(&self) -> usize {
        self.width
    }

    pub fn height(&self) -> usize {
        self.height
    }

    fn render<U: Copy>(&self, params: &DrawParams<U>) {
        unsafe {
            if let Some((fbo, targets)) = params.framebuffer {
                check!(gl::BindFramebuffer(gl::FRAMEBUFFER, fbo.handle()));
                check!(gl::DrawBuffers(targets.len() as _, targets.as_ptr()));
            }

            check!(gl::Viewport(
                params.viewport.0 as _,
                params.viewport.1 as _,
                params.viewport.2 as _,
                params.viewport.3 as _
            ));

            if let Some((r, g, b, a)) = params.clear_color {
                check!(gl::ClearColor(r, g, b, a));
            }

            if let Some(clear_buffers) = params.clear_buffers {
                check!(gl::Clear(clear_buffers));
            }

            if params.depth_test {
                check!(gl::Enable(gl::DEPTH_TEST));
            }

            if params.backface_culling {
                check!(gl::Enable(gl::CULL_FACE));
                check!(gl::CullFace(gl::BACK));
            }

            for (id, texture) in params.textures.iter().enumerate() {
                check!(gl::ActiveTexture(gl::TEXTURE0 + id as GLenum));
                check!(gl::BindTexture(gl::TEXTURE_2D, texture.handle()));
            }

            check!(gl::UseProgram(params.program.program()));
        }

        if let Some(ref uniforms) = params.uniforms {
            uniforms(&params.program, params.uniform_data);
        }

        unsafe {
            check!(gl::BindVertexArray(params.mesh.vao()));

            if params.wireframe {
                check!(gl::PolygonMode(gl::FRONT_AND_BACK, gl::LINE));
            }

            check!(gl::DepthMask(if params.depth_mask {
                gl::TRUE
            } else {
                gl::FALSE
            }));

            if params.alpha_blending {
                check!(gl::Enable(gl::BLEND));
                check!(gl::BlendFunc(gl::SRC_ALPHA, gl::ONE_MINUS_SRC_ALPHA));
            }

            if let Some(inst) = params.instances {
                check!(gl::DrawElementsInstanced(
                    params.mesh.primitive(),
                    params.mesh.indices() as _,
                    params.mesh.index_type(),
                    0 as _,
                    inst as _,
                ));
            } else {
                check!(gl::DrawElements(
                    params.mesh.primitive(),
                    params.mesh.indices() as _,
                    params.mesh.index_type(),
                    0 as _,
                ));
            }

            if params.alpha_blending {
                check!(gl::Disable(gl::BLEND));
            }

            if params.wireframe {
                check!(gl::PolygonMode(gl::FRONT_AND_BACK, gl::FILL));
            }

            check!(gl::BindVertexArray(0));
            check!(gl::UseProgram(0));

            if params.depth_test {
                check!(gl::Disable(gl::DEPTH_TEST));
            }

            if params.backface_culling {
                check!(gl::Disable(gl::CULL_FACE));
            }

            check!(gl::DepthMask(gl::TRUE));

            if let Some(fbo) = params.framebuffer {
                check!(gl::BindFramebuffer(gl::FRAMEBUFFER, 0));
            }
        }
    }

    pub fn upload_instancing<T: Instance>(&self, instances: &Vec<T>) {
        unsafe {
            check!(gl::BindBuffer(gl::ARRAY_BUFFER, self.instancing.handle()));
            check!(gl::BufferSubData(
                gl::ARRAY_BUFFER,
                0 as _,
                (mem::size_of::<T>() * instances.len()) as _,
                instances.as_ptr() as _
            ));
            check!(gl::BindBuffer(gl::ARRAY_BUFFER, 0));
        }
    }

    pub fn instancing(&self) -> &Instancing {
        &self.instancing
    }

    pub fn window(&self) -> &Window {
        &self.window
    }
}

pub fn render(
    renderer: &mut Renderer,
    debug: &Debug,
    lighting: &Lighting,
    shaders: &Shaders,
    textures: &Textures,
    framebufs: &Framebuffers,
    meshes: &Meshes,
    terrain: &Terrain,
    camera: &Camera,
    decals: &Decals,
    models: &Models,
    timer: &Timer,
    input: &Input,
) -> Result<()> {
    if !debug.render {
        return Ok(())
    }

    renderer.window.gl_swap_window();
    renderer.window.gl_make_current(&renderer.context).unwrap();

    let mut instances = Vec::with_capacity(1024);

    let shadow_ortho = Ortho {
        left: -128.0,
        right: 128.0,
        bottom: -128.0,
        top: 128.0,
        near: -64.0,
        far: 64.0,
    };

    let shadow_view = Matrix4::look_at_dir(
        Point3::new(0.0, 0.0, 0.0),
        lighting.sun_dir,
        Vector3::new(0.0, 1.0, 0.0),
    );

    let shadow_view_projection: Matrix4<f32> = Matrix4::from(shadow_ortho) * shadow_view;
    let view_culler = FrustumCuller::from_matrix(camera.view_projection());

    for x in -32..=32 {
        for z in -32..=32 {
            let aab = BoundingBox {
                min: Vector3::new(
                    16.0 * (x as f32) - 8.0,
                    -terrain.height() / 2.0,
                    16.0 * (z as f32) - 8.0,
                ),
                max: Vector3::new(
                    16.0 * (x as f32) + 8.0,
                    terrain.height() / 2.0,
                    16.0 * (z as f32) + 8.0,
                ),
            };

            if view_culler.test_bounding_box(aab) != Intersection::Outside {
                let translate = Vector3::new(16.0 * (x as f32), 0.0, 16.0 * (z as f32));
                instances.push(Matrix4::from_translation(translate));
            }
        }
    }

    // depth terrain
    if let (
        Some(fbo),
        Some(shadow_fbo),
        Some(patch),
        Some(&ShaderResult::Ok(ref program)),
        Some(heightfield),
    ) = (
        framebufs.get("depth_view"),
        framebufs.get("shadowmap"),
        meshes.get("patch"),
        shaders.get("depth_terrain_shader"),
        textures.get("heightfield"),
    ) {
        renderer.upload_instancing(&instances);

        if lighting.shadows {
            renderer.render(&DrawParams {
                clear_color: Some((0.0, 0.0, 0.0, 1.0)),
                clear_buffers: Some(gl::DEPTH_BUFFER_BIT),
                viewport: (0, 0, shadow_fbo.width(), shadow_fbo.height()),
                mesh: patch,
                framebuffer: Some((&shadow_fbo, &[])),
                textures: &[&heightfield],
                program: &program,
                uniform_data: (shadow_view_projection, &terrain),
                uniforms: Some(Box::new(|u, (s, t)| unsafe {
                    check!(gl::UniformMatrix4fv(
                        u.uniform("u_view_projection").unwrap(),
                        1,
                        gl::FALSE,
                        s.as_ptr() as _
                    ));
                    check!(gl::Uniform1i(u.uniform("u_heightfield").unwrap(), 0));
                    check!(gl::Uniform1f(u.uniform("u_height").unwrap(), t.height()));
                    check!(gl::Uniform1f(u.uniform("u_size").unwrap(), t.size() as _));
                })),
                depth_test: true,
                instances: Some(instances.len()),
                wireframe: false,
                backface_culling: true,
                depth_mask: true,
                alpha_blending: false,
                scissor: None,
            });
        }

        renderer.render(&DrawParams {
            clear_color: Some((0.0, 0.0, 0.0, 1.0)),
            clear_buffers: Some(gl::DEPTH_BUFFER_BIT),
            viewport: (0, 0, fbo.width(), fbo.height()),
            mesh: patch,
            framebuffer: Some((&fbo, &[gl::COLOR_ATTACHMENT0])),
            textures: &[&heightfield],
            program: &program,
            uniform_data: (&camera, &terrain),
            uniforms: Some(Box::new(|u, (c, t)| unsafe {
                let view_projection = c.projection() * c.view();
                check!(gl::UniformMatrix4fv(
                    u.uniform("u_view_projection").unwrap(),
                    1,
                    gl::FALSE,
                    view_projection.as_ptr() as _
                ));
                check!(gl::Uniform1i(u.uniform("u_heightfield").unwrap(), 0));
                check!(gl::Uniform1f(u.uniform("u_height").unwrap(), t.height()));
                check!(gl::Uniform1f(u.uniform("u_size").unwrap(), t.size() as _));
            })),
            depth_test: true,
            instances: Some(instances.len()),
            wireframe: false,
            backface_culling: true,
            depth_mask: true,
            alpha_blending: false,
            scissor: None,
        });
    }

    // depth models
    if let (Some(&ShaderResult::Ok(ref program)), Some(fbo), Some(shadow_fbo)) = (
        shaders.get("models_depth_shader"),
        framebufs.get("depth_view"),
        framebufs.get("shadowmap"),
    ) {
        for model in models.iter() {
            if let (Some(mesh), Some(texture)) =
                (meshes.get(model.mesh()), textures.get(model.texture()))
            {
                let mut instances = Vec::new();
                for instance in model.instances() {
                    instances.push(instance.clone());
                }

                renderer.upload_instancing(&instances);

                if lighting.shadows {
                    renderer.render(&DrawParams {
                        clear_color: None,
                        clear_buffers: None,
                        viewport: (0, 0, shadow_fbo.width(), shadow_fbo.height()),
                        mesh: mesh,
                        framebuffer: Some((&shadow_fbo, &[gl::COLOR_ATTACHMENT0])),
                        textures: &[],
                        program: &program,
                        uniform_data: shadow_view_projection,
                        uniforms: Some(Box::new(|u, s| unsafe {
                            check!(gl::UniformMatrix4fv(
                                u.uniform("u_view_projection").unwrap(),
                                1,
                                gl::FALSE,
                                s.as_ptr() as _
                            ));
                        })),
                        depth_test: true,
                        instances: Some(instances.len()),
                        wireframe: false,
                        backface_culling: true,
                        depth_mask: true,
                        alpha_blending: false,
                        scissor: None,
                    });
                }

                renderer.render(&DrawParams {
                    clear_color: None,
                    clear_buffers: None,
                    viewport: (0, 0, fbo.width(), fbo.height()),
                    mesh: mesh,
                    framebuffer: Some((&fbo, &[gl::COLOR_ATTACHMENT0])),
                    textures: &[],
                    program: &program,
                    uniform_data: &camera,
                    uniforms: Some(Box::new(|u, c| unsafe {
                        check!(gl::UniformMatrix4fv(
                            u.uniform("u_view_projection").unwrap(),
                            1,
                            gl::FALSE,
                            c.view_projection().as_ptr() as _
                        ));
                    })),
                    depth_test: true,
                    instances: Some(instances.len()),
                    wireframe: false,
                    backface_culling: true,
                    depth_mask: true,
                    alpha_blending: false,
                    scissor: None,
                });
            }
        }
    }

    if let (Some(fbo), Some(quad), Some(&ShaderResult::Ok(ref program))) = (
        framebufs.get("framebuffer"),
        meshes.get("quad"),
        shaders.get("sky_shader"),
    ) {
        renderer.render(&DrawParams {
            clear_color: Some((1.0, 0.0, 0.0, 1.0)),
            clear_buffers: Some(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT),
            viewport: (0, 0, fbo.width(), fbo.height()),
            mesh: quad,
            framebuffer: Some((&fbo, &[gl::COLOR_ATTACHMENT0, gl::COLOR_ATTACHMENT1])),
            textures: &[],
            program: &program,
            uniform_data: (),
            uniforms: None,
            depth_test: false,
            instances: None,
            wireframe: false,
            backface_culling: false,
            depth_mask: false,
            alpha_blending: false,
            scissor: None,
        });
    }

    // render terrain
    if let (
        true,
        Some(fbo),
        Some(shadow_fbo),
        Some(&ShaderResult::Ok(ref program)),
        Some(patch),
        Some(texture),
        Some(dirt),
        Some(concrete),
    ) = (
        debug.terrain,
        framebufs.get("framebuffer"),
        framebufs.get("shadowmap"),
        shaders.get("terrain_shader"),
        meshes.get("patch"),
        textures.get("heightfield"),
        textures.get("dirt"),
        textures.get("concrete"),
    ) {
        renderer.upload_instancing(&instances);

        renderer.render(&DrawParams {
            clear_color: None,
            clear_buffers: None,
            viewport: (0, 0, fbo.width(), fbo.height()),
            mesh: patch,
            framebuffer: Some((
                &fbo,
                &[
                    gl::COLOR_ATTACHMENT0,
                    gl::COLOR_ATTACHMENT1,
                    gl::COLOR_ATTACHMENT2,
                ],
            )),
            textures: &[&texture, &concrete, &dirt, shadow_fbo.depth().unwrap()],
            program: &program,
            uniform_data: (&camera, &terrain, &shadow_view_projection, &lighting),
            uniforms: Some(Box::new(|u, (c, t, s, l)| unsafe {
                check!(gl::UniformMatrix4fv(
                    u.uniform("u_projection").unwrap(),
                    1,
                    gl::FALSE,
                    c.projection().as_ptr() as _
                ));
                check!(gl::UniformMatrix4fv(
                    u.uniform("u_view").unwrap(),
                    1,
                    gl::FALSE,
                    c.view().as_ptr() as _
                ));
                check!(gl::UniformMatrix4fv(
                    u.uniform("u_shadow_view_projection").unwrap(),
                    1,
                    gl::FALSE,
                    s.as_ptr() as _
                ));
                check!(gl::Uniform1i(u.uniform("u_heightfield").unwrap(), 0));
                check!(gl::Uniform1i(u.uniform("u_concrete").unwrap(), 1));
                check!(gl::Uniform1i(u.uniform("u_dirt").unwrap(), 2));
                check!(gl::Uniform1i(u.uniform("u_shadowmap").unwrap(), 3));
                check!(gl::Uniform1f(u.uniform("u_height").unwrap(), t.height()));
                check!(gl::Uniform1f(u.uniform("u_size").unwrap(), t.size() as _));
                check!(gl::Uniform1i(u.uniform("u_shadows").unwrap(), if l.shadows { 1 } else { 0 }));

                let sun = l.sun_dir;
                check!(gl::Uniform3f(
                    u.uniform("u_sun_dir").unwrap(),
                    sun.x,
                    sun.y,
                    sun.z
                ));
                check!(gl::Uniform1f(
                    u.uniform("u_fog_density").unwrap(),
                    l.fog_density
                ));
            })),
            depth_test: true,
            instances: Some(instances.len()),
            wireframe: debug.wireframe,
            backface_culling: true,
            depth_mask: true,
            alpha_blending: false,
            scissor: None,
        });
    }

    if let (Some(fbo), Some(shadow_fbo), Some(&ShaderResult::Ok(ref program))) = (
        framebufs.get("framebuffer"),
        framebufs.get("shadowmap"),
        shaders.get("models_shader"),
    ) {
        for model in models.iter() {
            if let (Some(mesh), Some(texture)) =
                (meshes.get(model.mesh()), textures.get(model.texture()))
            {
                let mut instances = Vec::new();
                for instance in model.instances() {
                    instances.push(instance.clone());
                }

                renderer.upload_instancing(&instances);

                renderer.render(&DrawParams {
                    clear_color: None,
                    clear_buffers: None,
                    viewport: (0, 0, fbo.width(), fbo.height()),
                    mesh: mesh,
                    framebuffer: Some((
                        &fbo,
                        &[
                            gl::COLOR_ATTACHMENT0,
                            gl::COLOR_ATTACHMENT1,
                            gl::COLOR_ATTACHMENT2,
                        ],
                    )),
                    textures: &[&texture, shadow_fbo.depth().unwrap()],
                    program: &program,
                    uniform_data: (&camera, &shadow_view_projection, &lighting),
                    uniforms: Some(Box::new(|u, (c, s, l)| unsafe {
                        check!(gl::Uniform1i(u.uniform("u_texture").unwrap(), 0));
                        check!(gl::Uniform1i(u.uniform("u_shadowmap").unwrap(), 1));
                        check!(gl::Uniform1i(u.uniform("u_shadows").unwrap(), if l.shadows { 1 } else { 0 }));

                        let sun = l.sun_dir;
                        check!(gl::Uniform3f(
                            u.uniform("u_sun_dir").unwrap(),
                            sun.x,
                            sun.y,
                            sun.z
                        ));
                        check!(gl::Uniform1f(
                            u.uniform("u_fog_density").unwrap(),
                            l.fog_density
                        ));

                        check!(gl::UniformMatrix4fv(
                            u.uniform("u_shadow_view_projection").unwrap(),
                            1,
                            gl::FALSE,
                            s.as_ptr() as _
                        ));
                        check!(gl::UniformMatrix4fv(
                            u.uniform("u_projection").unwrap(),
                            1,
                            gl::FALSE,
                            c.projection().as_ptr() as _
                        ));
                        check!(gl::UniformMatrix4fv(
                            u.uniform("u_view").unwrap(),
                            1,
                            gl::FALSE,
                            c.view().as_ptr() as _
                        ));
                    })),
                    depth_test: true,
                    instances: Some(instances.len()),
                    wireframe: false,
                    backface_culling: true,
                    depth_mask: true,
                    alpha_blending: false,
                    scissor: None,
                });
            }
        }
    }

    if let (true, Some(fbo), Some(depth), Some(&ShaderResult::Ok(ref program)), Some(cube)) = (
        debug.decals,
        framebufs.get("framebuffer"),
        framebufs.get("depth_view").and_then(|d| d.depth()),
        shaders.get("decal_shader"),
        meshes.get("cube"),
    ) {
        for decal in decals.iter() {
            if let Some(texture) = textures.get(decal.texture()) {
                let mut instances = Vec::with_capacity(64);
                for inst in decal.instances() {
                    instances.push(*inst);
                }

                renderer.upload_instancing(&instances);

                renderer.render(&DrawParams {
                    clear_color: None,
                    clear_buffers: None,
                    viewport: (0, 0, fbo.width(), fbo.height()),
                    mesh: cube,
                    framebuffer: Some((&fbo, &[gl::COLOR_ATTACHMENT0])),
                    textures: &[&depth, &texture],
                    program: &program,
                    //uniform_data: (&camera, unsafe { mem::transmute::<_, Matrix4<f32>>(inst.clone()) }, fbo),
                    uniform_data: (&camera, fbo),
                    uniforms: Some(Box::new(|u, (c, fbo)| unsafe {
                        check!(gl::UniformMatrix4fv(
                            u.uniform("u_projection").unwrap(),
                            1,
                            gl::FALSE,
                            c.projection().as_ptr() as _
                        ));
                        check!(gl::UniformMatrix4fv(
                            u.uniform("u_view").unwrap(),
                            1,
                            gl::FALSE,
                            c.view().as_ptr() as _
                        ));
                        check!(gl::Uniform2f(
                            u.uniform("u_resolution").unwrap(),
                            fbo.width() as _,
                            fbo.height() as _
                        ));
                        check!(gl::Uniform1i(u.uniform("u_depth").unwrap(), 0));
                        check!(gl::Uniform1i(u.uniform("u_texture").unwrap(), 1));
                    })),
                    depth_test: true,
                    instances: Some(instances.len()),
                    wireframe: false,
                    backface_culling: true,
                    depth_mask: false,
                    alpha_blending: true,
                    scissor: None,
                });
            }
        }
    }

    if let (true, Some(fbo), Some(&ShaderResult::Ok(ref program)), Some(cube)) = (
        debug.debug_decals,
        framebufs.get("framebuffer"),
        shaders.get("solid_shader"),
        meshes.get("cube"),
    ) {
        for decal in decals.iter() {
            for inst in decal.instances() {
                renderer.render(&DrawParams {
                    clear_color: None,
                    clear_buffers: None,
                    viewport: (0, 0, fbo.width(), fbo.height()),
                    mesh: cube,
                    framebuffer: Some((&fbo, &[gl::COLOR_ATTACHMENT0])),
                    textures: &[],
                    program: &program,
                    uniform_data: (&camera, inst),
                    uniforms: Some(Box::new(|u, (c, t)| unsafe {
                        check!(gl::UniformMatrix4fv(
                            u.uniform("u_projection").unwrap(),
                            1,
                            gl::FALSE,
                            c.projection().as_ptr() as _
                        ));
                        check!(gl::UniformMatrix4fv(
                            u.uniform("u_view").unwrap(),
                            1,
                            gl::FALSE,
                            c.view().as_ptr() as _
                        ));
                        check!(gl::UniformMatrix4fv(
                            u.uniform("u_world").unwrap(),
                            1,
                            gl::FALSE,
                            t.as_ptr() as _
                        ));
                    })),
                    depth_test: true,
                    instances: None,
                    wireframe: true,
                    backface_culling: false,
                    depth_mask: true,
                    alpha_blending: false,
                    scissor: None,
                });
            }
        }
    }

    // SAO
    if let (
        true,
        Some(ssao_fbo),
        Some(color_fbo),
        Some(depth_fbo),
        Some(quad),
        Some(&ShaderResult::Ok(ref program)),
        Some(random),
    ) = (
        lighting.ao,
        framebufs.get("ssao"),
        framebufs.get("framebuffer"),
        framebufs.get("depth_view"),
        meshes.get("quad"),
        shaders.get("ssao"),
        textures.get("random"),
    ) {
        renderer.render(&DrawParams {
            clear_color: Some((1.0, 1.0, 1.0, 1.0)),
            clear_buffers: Some(gl::COLOR_BUFFER_BIT),
            viewport: (0, 0, ssao_fbo.width(), ssao_fbo.height()),
            mesh: quad,
            framebuffer: Some((&ssao_fbo, &[gl::COLOR_ATTACHMENT0])),
            textures: &[
                &color_fbo.target(2).unwrap(),
                &depth_fbo.depth().unwrap(),
                &random,
            ],
            program: &program,
            uniform_data: (&lighting, &camera),
            uniforms: Some(Box::new(|u, (l, c)| unsafe {
                check!(gl::Uniform1i(u.uniform("u_normal").unwrap(), 0));
                check!(gl::Uniform1i(u.uniform("u_depth").unwrap(), 1));
                check!(gl::Uniform1i(u.uniform("u_random").unwrap(), 2));

                // view projection & inv
                check!(gl::UniformMatrix4fv(
                    u.uniform("u_view_proj").unwrap(),
                    1,
                    gl::FALSE,
                    c.projection().as_ptr() as _
                ));
                check!(gl::UniformMatrix4fv(
                    u.uniform("u_inv_view_proj").unwrap(),
                    1,
                    gl::FALSE,
                    c.projection().invert().unwrap().as_ptr() as _
                ));

                check!(gl::Uniform1f(
                    u.uniform("u_ao_radius").unwrap(),
                    l.ao_radius,
                ));
                check!(gl::Uniform1f(
                    u.uniform("u_ao_factor").unwrap(),
                    l.ao_factor,
                ));
                check!(gl::Uniform1i(
                    u.uniform("u_ao_samples").unwrap(),
                    l.ao_samples as _,
                ));
            })),
            depth_test: false,
            instances: None,
            wireframe: false,
            backface_culling: false,
            depth_mask: false,
            alpha_blending: false,
            scissor: None,
        });
    }

    if let (
        Some(shadow_fbo),
        Some(fbo),
        Some(depth),
        Some(ssao),
        Some(quad),
        Some(&ShaderResult::Ok(ref program)),
        Some(random),
    ) = (
        framebufs.get("shadowmap"),
        framebufs.get("framebuffer"),
        framebufs.get("depth_view"),
        framebufs.get("ssao").and_then(|s| s.target(0)),
        meshes.get("quad"),
        shaders.get("post_shader"),
        textures.get("random"),
    ) {
        renderer.render(&DrawParams {
            clear_color: Some((0.5, 0.5, 0.5, 1.0)),
            clear_buffers: Some(gl::COLOR_BUFFER_BIT),
            viewport: (0, 0, renderer.width(), renderer.height()),
            mesh: quad,
            framebuffer: None,
            textures: &[&fbo.target(0).unwrap(), &fbo.target(1).unwrap(), &ssao],
            program: &program,
            uniform_data: (&lighting, &camera),
            uniforms: Some(Box::new(|u, (l, c)| unsafe {
                check!(gl::Uniform1i(u.uniform("u_texture").unwrap(), 0));
                check!(gl::Uniform1i(u.uniform("u_light").unwrap(), 1));
                check!(gl::Uniform1i(u.uniform("u_ssao").unwrap(), 2));
                check!(gl::Uniform1i(
                    u.uniform("u_ao_enabled").unwrap(),
                    if l.ao { 1 } else { 0 }
                ));

                let grading = l.color_grading;
                check!(gl::Uniform3f(
                    u.uniform("u_color_grading").unwrap(),
                    grading.x,
                    grading.y,
                    grading.z,
                ));
                check!(gl::Uniform1f(
                    u.uniform("u_min_shade").unwrap(),
                    l.min_shade,
                ));
                check!(gl::Uniform1f(
                    u.uniform("u_saturation").unwrap(),
                    l.saturation,
                ));
            })),
            depth_test: false,
            instances: None,
            wireframe: false,
            backface_culling: false,
            depth_mask: false,
            alpha_blending: false,
            scissor: None,
        });
    }

    Ok(())
}
