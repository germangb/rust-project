#version 450 core
#define {{shader}}

#ifdef VERTEX

layout(location = 0) in vec3 a_position;
layout(location = 1) in vec3 a_normal;
layout(location = 3) in mat4 a_world;

out mat4 v_inv_world;

uniform mat4 u_projection;
uniform mat4 u_view;

void main() {
    mat4 mvp = u_projection * u_view * a_world;

    gl_Position =  mvp * vec4(a_position, 1.0);

    v_inv_world = inverse(mvp);
}

#endif

#ifdef FRAGMENT

in mat4 v_inv_world;

out vec4 frag_color;

uniform sampler2D u_depth;
uniform sampler2D u_texture;

uniform vec2 u_resolution;

void main() {
    vec2 uv = gl_FragCoord.xy / u_resolution;

    float depth = texture(u_depth, uv).r * 2 - 1;

    vec4 local = v_inv_world * vec4(uv * 2 - 1, depth, 1);

    local.xyz /= local.w;

    vec4 mask = vec4(1.0);
    if (abs(local.x) > 1 || abs(local.y) > 1 || abs(local.z) > 1) {
        mask.a = 0.0;

        // Discarding frames has a performance impact. Use alpha blending instead...
        //discard;
    }

    frag_color = texture(u_texture, local.xz * 0.5 + 0.5) * mask;
}

#endif
