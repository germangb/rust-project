use {Error, Result};

use input::Input;

use sdl2::event::Event;
use sdl2::keyboard::{Keycode, Scancode};

#[derive(Debug)]
pub struct Debug {
    pub render: bool,
    pub terrain: bool,
    pub wireframe: bool,
    pub decals: bool,
    pub debug_decals: bool,
    pub debug_camera: bool,
    pub ignore_input: bool,
}

impl Default for Debug {
    fn default() -> Self {
        Debug {
            render: true,
            terrain: true,
            decals: true,
            wireframe: false,
            debug_decals: false,
            debug_camera: true,
            ignore_input: false,
        }
    }
}
