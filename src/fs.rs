use std::fs::File as StdFile;

use std::io::{Read, Result, Seek, SeekFrom};
use std::path::PathBuf;

lazy_static! {
    static ref BASE: &'static str = { "data/" };
}

pub struct File(StdFile);

impl File {
    pub fn open<P: Into<PathBuf>>(path: P) -> Result<File> {
        let mut data_path = PathBuf::from(*BASE);
        data_path.push(path.into());
        StdFile::open(data_path).map(|f| File(f))
    }
}

impl Read for File {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize> {
        self.0.read(buf)
    }
}

impl Seek for File {
    fn seek(&mut self, pos: SeekFrom) -> Result<u64> {
        self.0.seek(pos)
    }
}
