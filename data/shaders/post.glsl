#version 450 core
#define {{shader}}
#ifdef VERTEX

layout(location = 0) in vec3 a_position;

out vec2 v_uv;

void main() {
    gl_Position = vec4(a_position.xy, 0.0, 1.0);
    v_uv = a_position.xy * 0.5 + 0.5;
}

#endif

#ifdef FRAGMENT

in vec2 v_uv;

out vec4 frag_color;

uniform vec3 u_color_grading;
uniform float u_min_shade;
uniform float u_saturation;
uniform int u_ao_enabled = 0;

uniform sampler2D u_texture;
uniform sampler2D u_light;
uniform sampler2D u_ssao;

float rand(vec2 co){
    return fract(sin(dot(co.xy, vec2(12.9898,78.233))) * 43758.5453);
}

vec4 _texture_low(sampler2D sampler, vec2 uv) {
#define S(x,y,s) (texture(sampler, uv+tex*vec2(x,y))*s)

    vec2 tex = 1 / vec2(640, 480);
    vec4 a = S( 2,-2, 1) + S( 2,-1, 4) + S( 2,0, 7) + S( 2,1, 4) + S( 2,2,1) +
             S( 1,-2, 4) + S( 1,-1,16) + S( 1,0,26) + S( 1,1,16) + S( 1,2,4) +
             S( 0,-2, 7) + S( 0,-1,26) + S( 0,0,41) + S( 0,1,26) + S( 0,2,7) +
             S(-1,-2, 4) + S(-1,-1,16) + S(-1,0,26) + S(-1,1,16) + S(-1,2,4) +
             S(-2,-2, 1) + S(-2,-1, 4) + S(-2,0, 7) + S(-2,1, 4) + S(-2,2,1);

    return a / 273;
}

vec4 texture_low(sampler2D sampler, vec2 uv) {
#define T(x,y,s) (_texture_low(sampler, uv+tex*vec2(x,y))*s)

    vec2 tex = 2 / vec2(640, 480);
    vec4 a = T( 2,-2, 1) + T( 2,-1, 4) + T( 2,0, 7) + T( 2,1, 4) + T( 2,2,1) +
             T( 1,-2, 4) + T( 1,-1,16) + T( 1,0,26) + T( 1,1,16) + T( 1,2,4) +
             T( 0,-2, 7) + T( 0,-1,26) + T( 0,0,41) + T( 0,1,26) + T( 0,2,7) +
             T(-1,-2, 4) + T(-1,-1,16) + T(-1,0,26) + T(-1,1,16) + T(-1,2,4) +
             T(-2,-2, 1) + T(-2,-1, 4) + T(-2,0, 7) + T(-2,1, 4) + T(-2,2,1);

    return a / 273;
}

void main() {
    vec3 color = texture(u_texture, v_uv).rgb;
    vec4 normal_fog = texture(u_light, v_uv);

    // compute shade
    //float shade = dot(normalize(vec3(1, 3, 2)), normal_fog.xyz * 2 - 1);
    //float half_lamb = mix(0.4, 1.0, smoothstep(0., 1., shade));
    float shade = mix(u_min_shade, 1.0, smoothstep(0.0, 1.0, normal_fog.r));

    float ao = 1.0;
    if (u_ao_enabled == 1) {
        ao = smoothstep(0.0, 1.0, texture(u_ssao, v_uv).r);
        ao = texture(u_ssao, v_uv).r;
    }
    shade *= ao;

    float disp0 = rand(gl_FragCoord.xy) * 2 - 1;
    float dist = length(v_uv * 2 - 1 + disp0 * 0.025 * 0.0);

    vec3 tint = u_color_grading;
    //tint =  vec3(0.46, 0.4, 1.0);
    //tint =  vec3(1.0, 0.79, 0.76) * 1.1;

    color = mix(vec3(0.3), color * shade, normal_fog.w) * tint;

    // saturation
    float lum = dot(color, vec3(0.2126, 0.7152, 0.0722));
    float l = lum / dot(color, vec3(1.0));

    vec3 d = color - vec3(l);

    //color += d * u_saturation * 0.25;
    //color = vec3(lum) + d * u_saturation;

    frag_color = vec4(mix(color, color * 0.25, smoothstep(1.25, 1.5, length(dist))), 1.0);
    //frag_color = vec4(vec3(ao), 1.0);
}

#endif
