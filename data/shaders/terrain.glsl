#version 450 core

#define DEPTH {{depth}}
#define {{shader}}

#define COLOR

#ifdef VERTEX

#if DEPTH

layout(location = 0) in vec3 a_position;

#else   // DEPTH

layout(location = 0) in vec3 a_position;
layout(location = 1) in vec3 a_normal;
layout(location = 2) in vec2 a_uv;

#endif  // DEPTH

layout(location = 3) in mat4 a_world;

#if !DEPTH

out vec2 v_uv;
out vec3 v_position;
out vec3 v_position_view;
out vec3 v_normal;
out vec3 v_normal_view;
out vec4 v_shadow_projection;

#endif  // DEPTH

#if DEPTH

uniform mat4 u_view_projection;

#else   // DEPTH

uniform int u_shadows;
uniform mat4 u_shadow_view_projection;
uniform mat4 u_projection;
uniform mat4 u_view;

#endif  // DEPTH

uniform float u_height;
uniform float u_size;
uniform sampler2D u_heightfield;

vec3 map_terrain(vec3 p) {
    vec2 uv = p.xz / u_size + 0.5;
    p.y = (texture(u_heightfield, uv).r - 0.5) * u_height;
    return p;
}

#if DEPTH

void main() {
    vec4 world_vert = a_world * vec4(a_position, 1.0);
    world_vert.xyz = map_terrain(world_vert.xyz);
    gl_Position = u_view_projection * world_vert;
}

#else   // DEPTH

void main() {
    vec4 world_vert = a_world * vec4(a_position, 1.0);
    world_vert.xyz = map_terrain(world_vert.xyz);

    vec4 view_vert = u_view * world_vert;

    vec3 px = map_terrain(world_vert.xyz - vec3(4.0, 0.0, 0.0)) - map_terrain(world_vert.xyz + vec3(4.0, 0.0, 0.0));
    vec3 pz = map_terrain(world_vert.xyz - vec3(0.0, 0.0, 4.0)) - map_terrain(world_vert.xyz + vec3(0.0, 0.0, 4.0));

    v_normal = normalize(-cross(px, pz));
    v_normal_view = normalize((u_view * vec4(v_normal, 0.0)).xyz);

    v_position = world_vert.xyz;
    v_position_view = view_vert.xyz;
    v_uv = world_vert.xz / u_size + 0.5;

    if (u_shadows == 1) {
        v_shadow_projection = u_shadow_view_projection * world_vert;
    }

    gl_Position = u_projection * view_vert;
}

#endif   // DEPTH

#endif //VERTEX

#ifdef FRAGMENT

#define MIN(v) ((v.x<v.y)?(v.x):(v.y))

out vec4 frag_color;

#if DEPTH

void main() {
    frag_color = vec4(1.0);
}

#else

out vec4 light_fog;
out vec4 normals;

in vec2 v_uv;
in vec3 v_position;
in vec3 v_position_view;
in vec3 v_normal;
in vec3 v_normal_view;
in vec4 v_shadow_projection;

uniform int u_shadows;
uniform vec3 u_sun_dir;
uniform float u_fog_density;

uniform sampler2D u_heightfield;
uniform sampler2D u_concrete;
uniform sampler2D u_road;
uniform sampler2D u_dirt;
uniform sampler2D u_shadowmap;

float grid(float size, float sharp) {
    float gridx = smoothstep(0.0, sharp, abs(MIN(mod(v_position.xx*vec2(1.,-1.), size))));
    float gridz = smoothstep(0.0, sharp, abs(MIN(mod(v_position.zz*vec2(1.,-1.), size))));
    return 1.0 - (gridx * gridz);
}

float rand(vec2 co){
    return abs(fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453));
}

void main() {
    float h = texture(u_heightfield, v_uv).r;

    float fog_cont = exp(-length(v_position_view) * u_fog_density);
    float shade = dot(-normalize(u_sun_dir), v_normal);

    vec3 final_color = vec3(111, 119, 109) / 255.0;
    //vec3 final_color = vec3(156, 158, 129) / 255.0;

    float concrete = texture(u_concrete, v_uv).r;
    float road = texture(u_concrete, v_uv).g;
    float dirt = texture(u_dirt, v_uv).r;

    float grid_cont = grid(16.0, 0.15) * 0.5 + grid(1.0, 0.075) * 0.5 * max(0.0, concrete  - road);
    //grid_cont = 0;

    // draw dirt
    final_color = mix(final_color, vec3(130, 122, 111) / 255.0 * 1.45, smoothstep(-0.9, 1.0, dirt));
    final_color = mix(final_color, vec3(130, 122, 111) / 255.0 * 1.10, smoothstep( 0.0, 1.0, dirt + rand(v_uv * 512.0) * 0.5));

    // draw concrete
    float r = smoothstep(-1, 2, texture(u_heightfield, v_uv * 32.0).r);
    float s = smoothstep(-1, 2, texture(u_heightfield, v_uv * 32.0 + vec2(0.23, 0.512)).r);
    float c = 0.65;
    float d = 0.05;
    final_color = mix(final_color, mix(vec3(c), mix(vec3(c - d), vec3(c + d), s), r), smoothstep(0.48, 0.52, concrete));
    final_color = mix(final_color, vec3(0.70), smoothstep(0.08, 0.0, abs(concrete * 2.0 - 1.0)));

    // draw road
    final_color = mix(final_color, vec3(0.53), smoothstep(0.48, 0.52, road));
    final_color = mix(final_color, vec3(0.45), smoothstep(0.1, 0.0, abs(road * 2.0 - 1.0)));

    final_color = mix(final_color, final_color * 0.65, grid_cont * smoothstep(0.0, 1.0, texture(u_heightfield, v_uv * 8.0).r));

    vec3 shadow_test = v_shadow_projection.xyz;

    if (u_shadows == 1) {
        float shadow_shade = 1.0;
        if (abs(shadow_test.x) < 0.99 && abs(shadow_test.y) < 0.99 && abs(shadow_test.z) < 0.99) {
            float shadow_depth = texture2D(u_shadowmap, shadow_test.xy * 0.5 + 0.5).r;
            if (shadow_depth + 0.0001 < shadow_test.z * 0.5 + 0.5) {
                shadow_shade = 0;
            }
        }

        shade = min(shade, shadow_shade);
    }

    frag_color = vec4(final_color, 1.0);
    light_fog = vec4(clamp(shade, 0, 1), 0, 0, fog_cont);
    normals = vec4(v_normal_view * 0.5 + 0.5, 0.0);
}

#endif  // DEPTH

#endif  // FRAGMENT
