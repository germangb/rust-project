#version 450 core
#define {{shader}}

#ifdef VERTEX

layout(location = 0) in vec3 a_position;

uniform mat4 u_projection;
uniform mat4 u_view;
uniform mat4 u_world;

void main() {
    gl_Position = u_projection * u_view * u_world * vec4(a_position, 1.0);
}

#endif

#ifdef FRAGMENT

out vec4 frag_color;

void main() {
    frag_color = vec4(0, 0, 0, 1);
}

#endif
