use handlebars::Handlebars;

use std::result::Result as StdResult;
use {Error, Result};

use fs::File;

use toml;
use toml::from_str;

use std::io::Read;
use std::path::{Path, PathBuf};

use std::collections::hash_map::Iter as StdIter;
use std::collections::{BTreeMap, HashMap};
use std::ffi::CString;

use gl;
use gl::types::*;

use renderer::Renderer;

#[derive(Debug)]
pub struct Shader {
    source: String,
    program: GLuint,
    uniforms: HashMap<String, GLint>,
}

impl Drop for Shader {
    fn drop(&mut self) {
        unsafe { check!(gl::DeleteProgram(self.program)) }
    }
}

impl Shader {
    pub fn source(&self) -> &str {
        self.source.as_str()
    }

    pub fn program(&self) -> GLuint {
        self.program
    }

    pub fn uniform(&self, var: &str) -> Option<GLint> {
        self.uniforms.get(var).map(|u| *u)
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
struct ShaderToml {
    path: String,

    #[serde(default = "default_template")]
    template: BTreeMap<String, toml::Value>,

    #[serde(default = "default_uniforms")]
    uniforms: Vec<String>,
}

fn default_uniforms() -> Vec<String> {
    Vec::new()
}

fn default_template() -> BTreeMap<String, toml::Value> {
    BTreeMap::new()
}

type ShadersToml = BTreeMap<String, ShaderToml>;

#[derive(Debug)]
pub enum ShaderResult {
    Ok(Shader),
    Error {
        /// vertex shader compilation log
        vert: Option<String>,
        /// fragment shader compilation log
        frag: Option<String>,
    },
}

#[derive(Debug)]
pub struct Shaders {
    shaders: HashMap<String, ShaderResult>,

    #[cfg(debug)]
    toml: ShadersToml,
}

impl Shaders {
    pub fn open<P: AsRef<Path>>(_: &Renderer, path: P) -> Result<Self> {
        let mut file = File::open(path.as_ref())?;
        let mut contents = String::new();

        file.read_to_string(&mut contents)?;

        let toml: ShadersToml = from_str(contents.as_str()).unwrap();
        Self::from_toml(&toml)
    }

    fn from_toml(toml: &ShadersToml) -> Result<Self> {
        let mut shaders = HashMap::new();

        for (name, def) in toml.clone() {
            let mut file = File::open(&def.path)?;
            let mut shader = String::new();
            file.read_to_string(&mut shader)?;

            let shader = match create_program(shader, &def.uniforms, &def.template) {
                Ok(s) => s,
                Err(_) => continue,
            };
            shaders.insert(name.clone(), shader);
        }

        Ok(Shaders {
            shaders,

            #[cfg(debug)]
            toml: toml.clone(),
        })
    }

    #[cfg(debug)]
    pub fn reload_all(&mut self) -> Result<()> {
        let Self { shaders, .. } = Self::from_toml(&self.toml)?;

        self.shaders = shaders;
        Ok(())
    }

    pub fn get(&self, name: &str) -> Option<&ShaderResult> {
        self.shaders.get(name)
    }

    pub fn iter(&self) -> Iter {
        Iter(self.shaders.iter())
    }
}

#[derive(Debug)]
pub struct Iter<'a>(StdIter<'a, String, ShaderResult>);

impl<'a> Iterator for Iter<'a> {
    type Item = (&'a str, &'a ShaderResult);

    fn next(&mut self) -> Option<Self::Item> {
        self.0.next().map(|(n, s)| (n.as_str(), s))
    }
}

pub fn get_uniforms(program: GLuint, uniforms: &mut HashMap<String, GLint>) {
    unsafe {
        for (uniform, location) in uniforms.iter_mut() {
            let uniform = CString::new(uniform.as_str()).unwrap();

            *location = check!(gl::GetUniformLocation(program, uniform.as_ptr() as _));
        }
    }
}

fn create_program(
    shader: String,
    uniforms: &Vec<String>,
    template: &BTreeMap<String, toml::Value>,
) -> Result<ShaderResult> {
    unsafe {
        let vert = match create_shader(gl::VERTEX_SHADER, shader.as_str(), template) {
            Ok(s) => s,
            Err(log) => {
                eprintln!("{}", log);
                return Ok(ShaderResult::Error {
                    vert: Some(log),
                    frag: None,
                });
            }
        };
        let frag = match create_shader(gl::FRAGMENT_SHADER, shader.as_str(), template) {
            Ok(s) => s,
            Err(log) => {
                return Ok(ShaderResult::Error {
                    vert: None,
                    frag: Some(log),
                })
            }
        };
        let program = check!(gl::CreateProgram());

        check!(gl::AttachShader(program, vert));
        check!(gl::AttachShader(program, frag));
        check!(gl::LinkProgram(program));
        check!(gl::DeleteShader(vert));
        check!(gl::DeleteShader(frag));

        //let mut uniform_map = HashMap::new();
        let mut uniform_map = uniforms.iter().map(|s| (s.clone(), -1)).collect();
        get_uniforms(program, &mut uniform_map);

        Ok(ShaderResult::Ok(Shader {
            source: shader,
            program,
            uniforms: uniform_map,
        }))
    }
}

fn create_shader(
    type_: GLenum,
    source: &str,
    template: &BTreeMap<String, toml::Value>,
) -> StdResult<GLuint, String> {
    unsafe {
        let renderer = Handlebars::new();
        let mut render_data: HashMap<String, String> = template
            .clone()
            .into_iter()
            .map(|(k, v)| (k, format!("{}", v)))
            .collect();

        if type_ == gl::VERTEX_SHADER {
            render_data.insert("shader".to_string(), "VERTEX".to_string());
        } else if type_ == gl::FRAGMENT_SHADER {
            render_data.insert("shader".to_string(), "FRAGMENT".to_string());
        } else {
            unreachable!()
        }

        // render tempalte
        //println!("{:?}", render_data);
        let source = renderer.render_template(source, &render_data).unwrap();
        //println!("{}", source);
        let shader = gl::CreateShader(type_);

        check!(gl::ShaderSource(
            shader,
            1,
            [source.as_ptr() as _].as_ptr(),
            [source.len() as _].as_ptr()
        ));
        check!(gl::CompileShader(shader));

        let mut log_content = vec![0u8; 1024];
        let mut log_len = 0;

        check!(gl::GetShaderInfoLog(
            shader,
            log_content.len() as _,
            &mut log_len,
            log_content.as_mut_ptr() as _
        ));

        if log_len > 0 {
            let log_len = log_len as usize;
            //Err(Error::Other("Shader didn't compile"));

            let utf8 = &log_content[..log_len];
            Err(String::from_utf8_lossy(utf8).into_owned())

        //Err(String::from_utf8_lossy(&log_content[..log_len])
        //    .to_owned()
        //    .to_string())
        } else {
            Ok(shader)
        }
    }
}
