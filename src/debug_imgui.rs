use {Error, Result};

use imgui::{EditableColor, ImGui, ImGuiCond, ImString};
use imgui_gl_renderer::Renderer as ImGuiRenderer;
use imgui_sdl2::ImguiSdl2;

use camera::Camera;
use debug::Debug;
use decals::Decals;
use framebuffers::{Framebuffer, Framebuffers};
use input::Input;
use lighting::Lighting;
use models::Models;
use renderer::Renderer;
use shaders::{Shader, Shaders};
use terrain::Terrain;
use textures::Textures;
use time::Timer;

use std::mem;

pub struct ImguiTools {
    resources_tool: bool,
    metrics_tool: bool,
    checks_tool: bool,
    gui_tool: bool,
    render_tool: bool,
    graphics_tool: bool,
}

impl Default for ImguiTools {
    fn default() -> Self {
        Self {
            resources_tool: false,
            metrics_tool: false,
            checks_tool: false,
            gui_tool: false,
            render_tool: false,
            graphics_tool: false,
        }
    }
}

pub struct Imgui {
    imgui: ImGui,
    imgui_sdl2: ImguiSdl2,
    imgui_renderer: ImGuiRenderer,
}

impl Imgui {
    pub fn new() -> Self {
        let mut imgui = ImGui::init();
        let imgui_renderer = unsafe { ImGuiRenderer::init(&mut imgui) };
        let imgui_sdl2 = ImguiSdl2::new(&mut imgui);

        Self {
            imgui,
            imgui_sdl2,
            imgui_renderer,
        }
    }
}

pub fn render(
    imgui: &mut Imgui,
    tools: &mut ImguiTools,
    renderer: &Renderer,
    debug: &mut Debug,
    lighting: &mut Lighting,
    shaders: &mut Shaders,
    textures: &mut Textures,
    framebufs: &mut Framebuffers,
    terrain: &Terrain,
    camera: &Camera,
    decals: &mut Decals,
    models: &mut Models,
    timer: &Timer,
    input: &Input,
) -> Result<()> {
    for event in input.events() {
        imgui.imgui_sdl2.handle_event(&mut imgui.imgui, event);
    }
    let ui = imgui
        .imgui_sdl2
        .frame(&renderer.window(), &mut imgui.imgui, input.event_pump());

    ui.main_menu_bar(|| {
        ui.menu(im_str!("Tools")).build(|| {
            ui.menu_item(im_str!("Resources"))
                .selected(&mut tools.resources_tool)
                .build();
            ui.menu_item(im_str!("Metrics"))
                .selected(&mut tools.metrics_tool)
                .build();
            ui.menu_item(im_str!("Debug settings"))
                .selected(&mut tools.checks_tool)
                .build();
            ui.menu_item(im_str!("Rendering"))
                .selected(&mut tools.render_tool)
                .build();
            ui.menu_item(im_str!("Style"))
                .selected(&mut tools.gui_tool)
                .build();
        });
        ui.menu(im_str!("Rendering")).build(|| {
            ui.menu_item(im_str!("Graphics"))
                .selected(&mut tools.graphics_tool)
                .build();
            ui.menu_item(im_str!("Lighting"))
                .selected(&mut tools.render_tool)
                .build();
        });
    });

    if tools.metrics_tool {
        ui.show_metrics_window(&mut false);
    }

    if tools.gui_tool {
        ui.show_default_style_editor();
    }

    if tools.render_tool {
        ui.window(im_str!("Rendering settings"))
            .size((300.0, 100.0), ImGuiCond::FirstUseEver)
            .build(|| {
                ui.text(im_str!("Lightin"));

                unsafe {
                    let sun = ::std::mem::transmute(&mut lighting.sun_dir);
                    let grading = ::std::mem::transmute(&mut lighting.color_grading);

                    ui.checkbox(im_str!("Shadows"), &mut lighting.shadows);
                    ui.slider_float3(im_str!("Sun"), sun, -16.0, 16.0).build();
                    ui.slider_float(im_str!("Fog density"), &mut lighting.fog_density, 0.0, 0.1)
                        .build();
                    ui.slider_float(im_str!("Minimum shade"), &mut lighting.min_shade, 0.0, 1.0)
                        .build();
                    ui.separator();
                    ui.text(im_str!("Ambient Occlusion"));
                    ui.checkbox(im_str!("AO enabled"), &mut lighting.ao);
                    ui.slider_float(im_str!("AO radius"), &mut lighting.ao_radius, 0.0, 16.0)
                        .build();
                    ui.slider_float(im_str!("AO factor"), &mut lighting.ao_factor, 0.0, 8.0)
                        .build();
                    ui.slider_int(
                        im_str!("AO samples"),
                        mem::transmute(&mut lighting.ao_samples),
                        0,
                        128,
                    ).build();
                    ui.separator();
                    ui.text(im_str!("Color"));
                    ui.slider_float(im_str!("Saturation"), &mut lighting.saturation, 0.0, 4.0)
                        .build();
                    ui.color_picker(im_str!("Color grading"), EditableColor::Float3(grading))
                        .build();
                }
            });
    }

    if tools.checks_tool {
        ui.window(im_str!("Debug settings"))
            .size((300.0, 100.0), ImGuiCond::FirstUseEver)
            .build(|| {
                ui.checkbox(im_str!("Render"), &mut debug.render);
                ui.separator();
                ui.checkbox(im_str!("Ignore input"), &mut debug.ignore_input);
                ui.checkbox(im_str!("Debug camera"), &mut debug.debug_camera);
                ui.separator();
                ui.checkbox(im_str!("Render terrain"), &mut debug.terrain);
                ui.checkbox(im_str!("Wireframe"), &mut debug.wireframe);
                ui.separator();
                ui.checkbox(im_str!("Render decals"), &mut debug.decals);
                ui.checkbox(im_str!("Debug decals"), &mut debug.debug_decals);
            });
    }

    if tools.graphics_tool {
        ui.window(im_str!("Graphics"))
            .size((300.0, 200.0), ImGuiCond::FirstUseEver)
            .build(|| {
                if ui.button(im_str!("Reload Textures"), (140.0, 20.0)) {
                    textures.reload_all().unwrap();
                }
                if ui.button(im_str!("Reload Shaders"), (140.0, 20.0)) {
                    shaders.reload_all().unwrap();
                }
                if ui.button(im_str!("Reload Gltf"), (140.0, 20.0)) {}
                if ui.button(im_str!("Reload Meshes"), (140.0, 20.0)) {}
                if ui.button(im_str!("Reload Framebuffers"), (140.0, 20.0)) {}
            });
    }

    if tools.resources_tool {
        ui.window(im_str!("Resources"))
            .size((300.0, 200.0), ImGuiCond::FirstUseEver)
            .build(|| {
                ui.tree_node(im_str!("Decals")).build(|| {
                    for (i, _) in decals.iter().enumerate() {
                        ui.tree_node(&ImString::new(format!("decal #{}", i)))
                            .build(|| {
                                //ui.input_text("Transform",)
                            });
                    }
                });
            });
    }

    unsafe {
        imgui.imgui_renderer.render(ui);
    }

    Ok(())
}
