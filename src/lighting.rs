use cgmath::Vector3;

#[derive(Debug)]
pub struct Lighting {
    pub shadows: bool,
    pub sun_dir: Vector3<f32>,
    pub fog_density: f32,
    pub color_grading: Vector3<f32>,
    pub min_shade: f32,
    pub saturation: f32,

    pub ao: bool,
    pub ao_samples: u32,
    pub ao_radius: f32,
    pub ao_factor: f32,
}

impl Default for Lighting {
    fn default() -> Self {
        Lighting {
            shadows: true,
            sun_dir: Vector3::new(-1.0, -3.0, -2.0),

            fog_density: 0.01,
            color_grading: Vector3::new(1.000, 0.892, 0.702),
            min_shade: 0.7,
            saturation: 1.0,

            ao: true,
            ao_samples: 24,
            ao_radius: 4.0,
            ao_factor: 0.65,
        }
    }
}
