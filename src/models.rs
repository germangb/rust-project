use {Error, Result};

use fs::File;
use toml::from_str;

use cgmath::Matrix4;

use std::io::Read;
use std::mem;
use std::path::Path;
use std::slice::Iter as StdIter;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Instance {
    mesh: String,
    texture: String,
    #[serde(default = "default_aabb")]
    aabb: [f32; 6],
    instances: Vec<[f32; 16]>,
}

fn default_aabb() -> [f32; 6] {
    [-1.0, -1.0, -1.0, 1.0, 1.0, 1.0]
}

impl Instance {
    pub fn mesh(&self) -> &str {
        self.mesh.as_str()
    }

    pub fn texture(&self) -> &str {
        self.texture.as_str()
    }

    pub fn instances(&self) -> Instances {
        Instances(self.instances.iter())
    }
}

#[derive(Debug)]
pub struct Instances<'a>(StdIter<'a, [f32; 16]>);

impl<'a> Iterator for Instances<'a> {
    type Item = &'a Matrix4<f32>;

    fn next(&mut self) -> Option<Self::Item> {
        unsafe { self.0.next().map(|r| mem::transmute(r)) }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Models {
    instances: Vec<Instance>,
}

impl Models {
    pub fn open<P: AsRef<Path>>(path: P) -> Result<Self> {
        let mut file = File::open(path.as_ref())?;
        let mut contents = String::new();

        file.read_to_string(&mut contents)?;
        match from_str(contents.as_str()) {
            Ok(toml) => Ok(toml),
            Err(e) => Err(Error::Other("Deserialization error")),
        }
    }

    pub fn iter(&self) -> Iter {
        Iter(self.instances.iter())
    }
}

#[derive(Debug)]
pub struct Iter<'a>(StdIter<'a, Instance>);

impl<'a> Iterator for Iter<'a> {
    type Item = &'a Instance;

    #[inline]
    fn next(&mut self) -> Option<Self::Item> {
        self.0.next()
    }
}
