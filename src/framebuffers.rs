use {Error, Result};

use toml::from_str;

use renderer::Renderer;
use textures::{create_texture, Texture};

use fs::File;

use std::collections::HashMap;
use std::io::Read;
use std::path::{Path, PathBuf};

use gl;
use gl::types::*;

#[derive(Debug)]
pub struct Framebuffer {
    handle: GLuint,
    width: usize,
    height: usize,
    targets: Vec<Texture>,
    depth: Option<Texture>,
}

impl Drop for Framebuffer {
    fn drop(&mut self) {
        unsafe { check!(gl::DeleteFramebuffers(1, &mut self.handle)) }
    }
}

impl Framebuffer {
    pub fn handle(&self) -> GLuint {
        self.handle
    }

    pub fn width(&self) -> usize {
        self.width
    }

    pub fn height(&self) -> usize {
        self.height
    }

    pub fn targets(&self) -> &[Texture] {
        self.targets.as_ref()
    }

    pub fn target(&self, idx: usize) -> Option<&Texture> {
        self.targets.get(idx)
    }

    pub fn depth(&self) -> Option<&Texture> {
        self.depth.as_ref()
    }
}

#[derive(Debug, Serialize, Deserialize)]
struct FramebufferToml {
    #[serde(default = "default_size")]
    width: usize,
    #[serde(default = "default_size")]
    height: usize,
    #[serde(default = "default_scale")]
    scale: f32,
    targets: usize,
    depth: bool,
}

fn default_scale() -> f32 {
    1.0
}

fn default_size() -> usize {
    0
}

type FramebuffersToml = HashMap<String, FramebufferToml>;

#[derive(Debug)]
pub struct Framebuffers(HashMap<String, Framebuffer>);

impl Framebuffers {
    pub fn open<P: AsRef<Path>>(renderer: &Renderer, path: P) -> Result<Self> {
        let mut file = File::open(path.as_ref())?;
        let mut contents = String::new();

        file.read_to_string(&mut contents)?;

        let mut fbos = HashMap::new();

        let toml_defs: FramebuffersToml = from_str(contents.as_str()).unwrap();
        for (name, def) in toml_defs {
            let scale = def.scale;
            let width = if def.width > 0 { def.width } else { renderer.width() } as f32;
            let height = if def.height > 0 { def.height } else { renderer.height() } as f32;
            fbos.insert(
                name,
                load_framebuffer((width * scale) as usize, (height * scale) as usize, def.targets, def.depth)?,
            );
        }

        Ok(Framebuffers(fbos))
    }

    pub fn get(&self, name: &str) -> Option<&Framebuffer> {
        self.0.get(name)
    }
}

fn load_framebuffer(
    width: usize,
    height: usize,
    targets: usize,
    depth: bool,
) -> Result<Framebuffer> {
    let mut fbo = Framebuffer {
        handle: 0,
        width,
        height,
        targets: Vec::with_capacity(targets),
        depth: None,
    };

    use std::ptr;

    unsafe {
        check!(gl::GenFramebuffers(1, &mut fbo.handle));
        check!(gl::BindFramebuffer(gl::FRAMEBUFFER, fbo.handle));

        // create textures
        for id in 0..targets {
            let texture = create_texture(
                ptr::null(),
                width,
                height,
                gl::RGBA16,
                gl::RGBA,
                gl::UNSIGNED_BYTE,
                gl::CLAMP_TO_EDGE,
            ).unwrap();

            check!(gl::FramebufferTexture2D(
                gl::FRAMEBUFFER,
                gl::COLOR_ATTACHMENT0 + id as GLenum,
                gl::TEXTURE_2D,
                texture.handle(),
                0
            ));

            fbo.targets.push(texture);
        }

        if depth {
            let depth = create_texture(
                ptr::null(),
                width,
                height,
                gl::DEPTH_COMPONENT24,
                gl::DEPTH_COMPONENT,
                gl::UNSIGNED_BYTE,
                gl::CLAMP_TO_EDGE,
            ).unwrap();

            check!(gl::FramebufferTexture2D(
                gl::FRAMEBUFFER,
                gl::DEPTH_ATTACHMENT,
                gl::TEXTURE_2D,
                depth.handle(),
                0
            ));

            fbo.depth = Some(depth);
        }

        // check the validity of the fbo
        let error = check!(gl::CheckFramebufferStatus(gl::FRAMEBUFFER));
        if error != gl::FRAMEBUFFER_COMPLETE {
            check!(gl::BindFramebuffer(gl::FRAMEBUFFER, 0));
            return Err(Error::Other("Framebuffer not complete"));
        }

        check!(gl::BindFramebuffer(gl::FRAMEBUFFER, 0));
    }

    Ok(fbo)
}
