use {Error, Result};

use toml::from_str;

use std::collections::{BTreeMap, HashMap};
use std::io::Read;
use std::path::Path;

use fs::File;

use gltf::Gltf;
use renderer::{Instancing, Renderer};

use gltf_crate as gltf;

use gl;
use gl::types::*;

#[derive(Debug)]
pub struct Mesh {
    buffers: Vec<GLuint>,
    vao: GLuint,
    primitive: GLenum,
    index_type: GLenum,
    indices: usize,
}

impl Drop for Mesh {
    fn drop(&mut self) {
        unsafe {
            check!(gl::DeleteBuffers(
                self.buffers.len() as _,
                self.buffers.as_ptr()
            ));
            check!(gl::DeleteVertexArrays(1, &self.vao));
        }
    }
}

impl Mesh {
    pub fn vao(&self) -> GLuint {
        self.vao
    }

    pub fn primitive(&self) -> GLenum {
        self.primitive
    }

    pub fn index_type(&self) -> GLenum {
        self.index_type
    }

    pub fn indices(&self) -> usize {
        self.indices
    }
}

#[derive(Debug)]
pub struct Meshes {
    meshes: HashMap<String, Mesh>,
}

#[derive(Debug, Deserialize)]
struct MeshToml {
    gltf: String,
    id: usize,
    #[serde(default = "default_instanced")]
    instanced: bool,
}

fn default_instanced() -> bool {
    false
}

type MeshesToml = BTreeMap<String, MeshToml>;

impl Meshes {
    pub fn open<P: AsRef<Path>>(gltf: &Gltf, renderer: &Renderer, path: P) -> Result<Self> {
        let mut file = File::open(path.as_ref())?;
        let mut contents = String::new();

        file.read_to_string(&mut contents)?;
        let toml_defs: MeshesToml =
            from_str(contents.as_str()).map_err(|_| Error::Other("Error reading meshes toml"))?;

        let mut meshes = HashMap::new();
        for (name, def) in toml_defs {
            meshes.insert(name, create_mesh(&def, gltf, renderer.instancing())?);
        }

        Ok(Meshes { meshes })
    }

    pub fn get(&self, name: &str) -> Option<&Mesh> {
        self.meshes.get(name)
    }
}

fn create_mesh(toml: &MeshToml, gltf: &Gltf, instancing: &Instancing) -> Result<Mesh> {
    let (gltf_path, gltf) = gltf
        .get(&toml.gltf)
        .ok_or(Error::Other("gltf asset is not defined"))?;

    let mesh = gltf
        .meshes()
        .nth(toml.id)
        .ok_or(Error::Other("Error getting nth mesh from gltf"))?;
    let primitive = mesh.primitives().next().ok_or(Error::Other(
        "Error gerring the first primitive of the mesh",
    ))?;

    let indices = primitive.indices().unwrap();
    let index_data_type = match indices.data_type() {
        gltf::accessor::DataType::I8 => gl::BYTE,
        gltf::accessor::DataType::U8 => gl::UNSIGNED_BYTE,
        gltf::accessor::DataType::I16 => gl::SHORT,
        gltf::accessor::DataType::U16 => gl::UNSIGNED_SHORT,
        gltf::accessor::DataType::U32 => gl::UNSIGNED_INT,

        _ => return Err(Error::Other("Invalid index data type")),
    };

    let index_count = indices.size() * indices.count();

    let buffers: Vec<Vec<u8>> = gltf
        .buffers()
        .map(|b| match b.source() {
            gltf::buffer::Source::Uri(uri) => {
                let mut path = gltf_path.clone();
                path.pop();
                path.push(uri);

                let mut buffer = Vec::new();
                let mut path = File::open(path).unwrap();
                path.read_to_end(&mut buffer).unwrap();

                buffer
            }
            _ => panic!(),
        })
        .collect();

    unsafe {
        let mut vao = 0;

        check!(gl::GenVertexArrays(1, &mut vao));
        check!(gl::BindVertexArray(vao));

        // gen as many buffer as buffer views there are on the glTF document
        let mut gl_buffers = vec![0; gltf.views().len()];
        check!(gl::GenBuffers(
            gl_buffers.len() as _,
            gl_buffers.as_mut_ptr()
        ));

        for (buffer, view) in gl_buffers.iter().zip(gltf.views()) {
            let target = match view.target() {
                Some(gltf::buffer::Target::ArrayBuffer) => gl::ARRAY_BUFFER,
                Some(gltf::buffer::Target::ElementArrayBuffer) => gl::ELEMENT_ARRAY_BUFFER,
                _ => panic!(),
            };
            check!(gl::BindBuffer(target, *buffer));

            let o = view.offset();
            let l = view.length();

            let buffer = &buffers[view.buffer().index()][o..(o + l)];
            check!(gl::BufferData(
                target,
                buffer.len() as _,
                buffer.as_ptr() as _,
                gl::STATIC_DRAW
            ));
            //eprintln!("{}, {}", o, l);
        }

        if let Some(p) = primitive.get(&gltf::Semantic::Positions) {
            check!(gl::BindBuffer(
                gl::ARRAY_BUFFER,
                gl_buffers[p.view().index()]
            ));
            check!(gl::EnableVertexAttribArray(0));
            check!(gl::VertexAttribPointer(
                0,
                3,
                gl::FLOAT,
                gl::FALSE,
                p.view().stride().unwrap_or(0) as _,
                p.offset() as _
            ));
        }

        if let Some(p) = primitive.get(&gltf::Semantic::Normals) {
            check!(gl::BindBuffer(
                gl::ARRAY_BUFFER,
                gl_buffers[p.view().index()]
            ));
            check!(gl::EnableVertexAttribArray(1));
            check!(gl::VertexAttribPointer(
                1,
                3,
                gl::FLOAT,
                gl::FALSE,
                p.view().stride().unwrap_or(0) as _,
                p.offset() as _
            ));
        }

        if let Some(p) = primitive.get(&gltf::Semantic::TexCoords(0)) {
            check!(gl::BindBuffer(
                gl::ARRAY_BUFFER,
                gl_buffers[p.view().index()]
            ));
            check!(gl::EnableVertexAttribArray(2));
            check!(gl::VertexAttribPointer(
                2,
                2,
                gl::FLOAT,
                gl::FALSE,
                p.view().stride().unwrap_or(0) as _,
                p.offset() as _
            ));
        }

        if toml.instanced {
            check!(gl::BindBuffer(gl::ARRAY_BUFFER, instancing.handle()));
            check!(gl::EnableVertexAttribArray(3));
            check!(gl::EnableVertexAttribArray(4));
            check!(gl::EnableVertexAttribArray(5));
            check!(gl::EnableVertexAttribArray(6));
            check!(gl::VertexAttribPointer(
                3,
                4,
                gl::FLOAT,
                gl::FALSE,
                16 << 2,
                (0 << 2) as _
            ));
            check!(gl::VertexAttribPointer(
                4,
                4,
                gl::FLOAT,
                gl::FALSE,
                16 << 2,
                (4 << 2) as _
            ));
            check!(gl::VertexAttribPointer(
                5,
                4,
                gl::FLOAT,
                gl::FALSE,
                16 << 2,
                (8 << 2) as _
            ));
            check!(gl::VertexAttribPointer(
                6,
                4,
                gl::FLOAT,
                gl::FALSE,
                16 << 2,
                (12 << 2) as _
            ));
            check!(gl::VertexAttribDivisor(3, 1));
            check!(gl::VertexAttribDivisor(4, 1));
            check!(gl::VertexAttribDivisor(5, 1));
            check!(gl::VertexAttribDivisor(6, 1));
        } else {
            check!(gl::VertexAttribDivisor(3, 0));
            check!(gl::VertexAttribDivisor(4, 0));
            check!(gl::VertexAttribDivisor(5, 0));
            check!(gl::VertexAttribDivisor(6, 0));
        }

        if let Some(p) = primitive.get(&gltf::Semantic::Joints(0)) {
            check!(gl::BindBuffer(
                gl::ARRAY_BUFFER,
                gl_buffers[p.view().index()]
            ));
            check!(gl::EnableVertexAttribArray(7));
            check!(gl::VertexAttribPointer(
                7,
                4,
                gl::FLOAT,
                gl::FALSE,
                p.view().stride().unwrap_or(0) as _,
                p.offset() as _
            ));
        }

        if let Some(p) = primitive.get(&gltf::Semantic::Weights(0)) {
            check!(gl::BindBuffer(
                gl::ARRAY_BUFFER,
                gl_buffers[p.view().index()]
            ));
            check!(gl::EnableVertexAttribArray(8));
            check!(gl::VertexAttribPointer(
                8,
                4,
                gl::FLOAT,
                gl::FALSE,
                p.view().stride().unwrap_or(0) as _,
                p.offset() as _
            ));
        }

        check!(gl::BindVertexArray(0));
        check!(gl::BindBuffer(gl::ARRAY_BUFFER, 0));
        check!(gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, 0));
        check!(gl::DisableVertexAttribArray(0));
        check!(gl::DisableVertexAttribArray(1));
        check!(gl::DisableVertexAttribArray(2));
        check!(gl::DisableVertexAttribArray(3));
        check!(gl::DisableVertexAttribArray(4));
        check!(gl::DisableVertexAttribArray(5));
        check!(gl::DisableVertexAttribArray(6));
        check!(gl::DisableVertexAttribArray(7));
        check!(gl::DisableVertexAttribArray(8));

        Ok(Mesh {
            buffers: gl_buffers,
            vao,
            primitive: primitive.mode().as_gl_enum(),
            index_type: index_data_type,
            indices: index_count,
        })
    }
}
