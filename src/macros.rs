#[macro_export]
macro_rules! check {
    ($gl:expr) => {{
        let result = $gl;

        let error = $crate::gl::GetError();
        if error != $crate::gl::NO_ERROR {
            panic!("[GL-ERROR] GL ERROR: {}", error);
        }

        result
    }};
}
