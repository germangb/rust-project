use {Error, Result};

use cgmath::prelude::*;
use cgmath::{Euler, Matrix4, PerspectiveFov, Rad, Vector3, Vector4};

use cgmath_culling::{BoundingBox, FrustumCuller, Intersection, Sphere};

use input::Input;
use time::Timer;

use camera::{Camera, Position, Rotation};
use debug::Debug;


pub fn update(camera: &mut Camera, input: &Input, timer: &Timer, debug: &Debug) -> Result<()> {
    use sdl2::keyboard::Scancode;

    if debug.ignore_input {
        return Ok(());
    }

    let keyboard = input.keyboard_state();
    let mouse = input.mouse_state();
    let rel_mouse = input.relative_mouse_state();

    let up = Vector3::new(0.0, 1.0, 0.0);
    let mut right = camera.right().clone();
    let mut look = up.cross(right);
    look.y = 0.0;
    right.y = 0.0;
    let velocity = 16.0 * timer.delta_time();

    if mouse.left() {
        let dx = rel_mouse.x() as f32;
        let dy = rel_mouse.y() as f32;

        camera.update_view(
            Some(Rotation::AddEuler(Euler {
                x: Rad(dy * 0.01),
                y: Rad(dx * 0.01),
                z: Rad(0.0),
            })),
            None,
        );
    }

    if keyboard.is_scancode_pressed(Scancode::Space) {
        camera.update_view(None, Some(Position::Add(up * velocity)));
    }

    if keyboard.is_scancode_pressed(Scancode::LShift) {
        camera.update_view(None, Some(Position::Add(-up * velocity)));
    }

    if keyboard.is_scancode_pressed(Scancode::W) {
        camera.update_view(None, Some(Position::Add(look * velocity)));
    }

    if keyboard.is_scancode_pressed(Scancode::S) {
        camera.update_view(None, Some(Position::Add(-look * velocity)));
    }

    if keyboard.is_scancode_pressed(Scancode::D) {
        camera.update_view(None, Some(Position::Add(right * velocity)));
    }

    if keyboard.is_scancode_pressed(Scancode::A) {
        camera.update_view(None, Some(Position::Add(-right * velocity)));
    }

    // update transforms
    camera.commit();

    Ok(())
}
