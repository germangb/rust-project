#version 450 core

#define {{shader}}
#define DEPTH {{depth}}

#ifdef VERTEX

#if DEPTH

layout(location = 0) in vec3 a_position;
layout(location = 3) in mat4 a_world;

#else   // DEPTH

layout(location = 0) in vec3 a_position;
layout(location = 1) in vec3 a_normal;
layout(location = 2) in vec2 a_uv;
layout(location = 3) in mat4 a_world;

#endif  // DEPTH

#if DEPTH

out vec2 v_uv;
out vec3 v_normal;
out vec3 v_position_view;

#else   // DEPTH

out vec2 v_uv;
out vec3 v_normal;
out vec3 v_normal_view;
out vec3 v_position_view;
out vec4 v_shadow_projection;

#endif  // DEPTH

#if DEPTH

uniform mat4 u_view_projection;

#else   // DEPTH

uniform int u_shadows;
uniform mat4 u_shadow_view_projection;
uniform mat4 u_projection;
uniform mat4 u_view;

#endif  // DEPTH

#if DEPTH

void main() {
    gl_Position = u_view_projection * a_world * vec4(a_position, 1.0);
}

#else   // DEPTH

void main() {
    gl_Position = u_projection * u_view * a_world * vec4(a_position, 1.0);

    if (u_shadows == 1) {
        v_shadow_projection = u_shadow_view_projection * a_world * vec4(a_position, 1.0);
    }
    v_position_view = (u_view * a_world * vec4(a_position, 1.0)).xyz;
    v_normal = normalize((a_world * vec4(a_normal, 0.0)).xyz);
    v_normal_view = normalize((u_view * vec4(v_normal, 0.0)).xyz);
    v_uv = a_uv;
}

#endif  // DEPTH

#endif  // VERTEX

#ifdef FRAGMENT

#if DEPTH

out vec4 frag_color;

void main() {
    frag_color = vec4(1.0);
}

#else  // DEPH

in vec3 v_normal;
in vec3 v_normal_view;
in vec2 v_uv;
in vec3 v_position_view;
in vec4 v_shadow_projection;

out vec4 frag_color;
out vec4 light_fog;
out vec4 normals;

uniform int u_shadows;
uniform vec3 u_sun_dir;
uniform float u_fog_density;

uniform sampler2D u_texture;
uniform sampler2D u_shadowmap;

float rand(vec2 co){
    return abs(fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453));
}

void main() {
    float fog_cont = exp(-length(v_position_view) * u_fog_density);
    float shade = dot(-normalize(u_sun_dir), v_normal);

    if (u_shadows == 1) {
        vec3 shadow_test = v_shadow_projection.xyz;
        float shadow_shade = 1.0;
        if (abs(shadow_test.x) < 0.99 && abs(shadow_test.y) < 0.99) {
            float shadow_depth = texture2D(u_shadowmap, shadow_test.xy * 0.5 + 0.5).r;

            if (shadow_depth + 0.0001 < shadow_test.z * 0.5 + 0.5) {
                shadow_shade = 0;
            }
        }
        shade = min(shade, shadow_shade);
    }

    frag_color = texture(u_texture, v_uv);
    light_fog = vec4(clamp(shade, 0.0, 1.0), 0.0, 0.0, fog_cont);
    normals = vec4(v_normal_view * 0.5 + 0.5, 0.0);
}

#endif  // DEPTH

#endif  // FRAGMENT
