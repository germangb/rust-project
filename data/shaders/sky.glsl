#version 450 core
#define {{shader}}

#ifdef VERTEX

layout(location = 0) in vec3 a_position;

out vec2 v_uv;

void main() {
    gl_Position = vec4(a_position, 1.0);
    v_uv = a_position.xy;
}

#endif

#ifdef FRAGMENT

in vec2 v_uv;

out vec4 frag_color;

void main() {
    vec3 color = vec3(0.3);
    vec3 sky = mix(color, color, 1-v_uv.y);
    frag_color = vec4(sky, 0);
}

#endif
