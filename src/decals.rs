use {Error, Result};

use fs::File;
use toml::from_str;

use std::io::Read;
use std::mem;
use std::path::Path;
use std::slice::Iter as StdIter;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Instance {
    texture: String,
    instances: Vec<[f32; 16]>,
}

impl Instance {
    pub fn texture(&self) -> &str {
        self.texture.as_str()
    }

    pub fn instances(&self) -> Instances {
        Instances(self.instances.iter())
    }
}

#[derive(Debug)]
pub struct Instances<'a>(StdIter<'a, [f32; 16]>);

impl<'a> Iterator for Instances<'a> {
    type Item = &'a [f32; 16];

    fn next(&mut self) -> Option<Self::Item> {
        self.0.next()
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Decals {
    instances: Vec<Instance>,
}

impl Decals {
    pub fn open<P: AsRef<Path>>(path: P) -> Result<Self> {
        let mut file = File::open(path.as_ref())?;
        let mut contents = String::new();

        file.read_to_string(&mut contents)?;
        match from_str(contents.as_str()) {
            Ok(toml) => Ok(toml),
            Err(e) => Err(Error::Other("Deserialization error")),
        }
    }

    pub fn iter(&self) -> Iter {
        Iter(self.instances.iter())
    }
}

#[derive(Debug)]
pub struct Iter<'a>(StdIter<'a, Instance>);

impl<'a> Iterator for Iter<'a> {
    type Item = &'a Instance;

    #[inline]
    fn next(&mut self) -> Option<Self::Item> {
        self.0.next()
    }
}
