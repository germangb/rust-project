#[macro_use]
extern crate lazy_static;
extern crate gltf as gltf_crate;
extern crate handlebars;

extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate toml;

extern crate cgmath;
extern crate cgmath_culling;
extern crate gl;
extern crate sdl2;
extern crate stb;

#[macro_use]
#[cfg(debug)]
extern crate imgui;
#[cfg(debug)]
extern crate imgui_gl_renderer;
#[cfg(debug)]
extern crate imgui_sdl2;

mod fs;
mod terrain;
#[macro_use]
mod macros;
mod framebuffers;
mod gltf;
mod lighting;
mod meshes;
mod renderer;
mod shaders;
mod textures;

#[cfg(debug)]
mod debug_imgui;

mod camera;
mod camera_debug;

mod collisions;
mod debug;
mod decals;
mod input;
mod models;
mod time;

use input::System as InputSys;
use time::System as TimeSys;

use debug::Debug;

#[cfg(debug)]
use debug_imgui::{Imgui, ImguiTools};

use input::Input;
use lighting::Lighting;
use renderer::Renderer;
use terrain::Terrain;

use decals::Decals;
use models::Models;

use framebuffers::Framebuffers;
use gltf::Gltf;
use meshes::Meshes;
use shaders::Shaders;
use textures::Textures;

use time::Timer;
use camera::Camera;

use cgmath::{PerspectiveFov, Rad};

#[derive(Debug)]
pub enum Error {
    AlreadyClosed,
    Io(::std::io::Error),
    Other(&'static str),
}

impl From<::std::io::Error> for Error {
    fn from(e: ::std::io::Error) -> Error {
        Error::Io(e)
    }
}

pub type Result<T> = std::result::Result<T, Error>;

fn main() -> Result<()> {
    let sdl = sdl2::init().unwrap();
    let video = sdl.video().unwrap();
    let timer = sdl.timer().unwrap();
    let events = sdl.event_pump().unwrap();

    let mut debug = Debug::default();
    let mut decals = Decals::open("decals.toml")?;
    let mut models = Models::open("models.toml")?;

    let (width, height) = (640, 480);

    let mut renderer = Renderer::new(video, width, height);
    let mut shaders = Shaders::open(&renderer, "shaders.toml")?;
    let mut textures = Textures::open(&renderer, "textures.toml")?;
    let mut framebufs = Framebuffers::open(&renderer, "framebuf.toml")?;
    let mut gltf = Gltf::open("gltf.toml")?;
    let mut meshes = Meshes::open(&gltf, &renderer, "meshes.toml")?;

    #[cfg(debug)]
    let (mut imgui, mut imgui_tools) = (Imgui::new(), ImguiTools::default());

    let mut input = Input::new(events);
    let mut timer = Timer::new(timer);

    let mut lighting = Lighting::default();

    let mut camera = Camera::new(PerspectiveFov {
        fovy: Rad(0.93),
        aspect: (width as f32) / (height as f32),
        near: 0.01,
        far: 512.0,
    });

    let terrain = Terrain::new(16.0, 1024);

    loop {
        TimeSys::update(&mut timer)?;
        InputSys::update(&mut input)?;

        camera_debug::update(&mut camera, &input, &timer, &debug)?;

        renderer::render(
            &mut renderer,
            &debug,
            &lighting,
            &shaders,
            &textures,
            &framebufs,
            &meshes,
            &terrain,
            &camera,
            &decals,
            &models,
            &timer,
            &input,
        )?;

        #[cfg(debug)]
        debug_imgui::render(
            &mut imgui,
            &mut imgui_tools,
            &renderer,
            &mut debug,
            &mut lighting,
            &mut shaders,
            &mut textures,
            &mut framebufs,
            &terrain,
            &camera,
            &mut decals,
            &mut models,
            &timer,
            &input,
        )?;
    }
}
