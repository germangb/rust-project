use {Error, Result};

use std::collections::hash_map::Iter;
use std::collections::HashMap;

use std::io::Read;
use std::path::{Path, PathBuf};

use fs::File;
use toml::from_str;

use stb::image;
use stb::image::Image;

use gl;
use gl::types::*;

use renderer::Renderer;

#[derive(Clone, Debug)]
pub struct Texture {
    handle: GLuint,
}

impl Drop for Texture {
    fn drop(&mut self) {
        unsafe { check!(gl::DeleteTextures(1, &mut self.handle)) }
    }
}

impl Texture {
    pub fn handle(&self) -> GLuint {
        self.handle
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
struct TextureToml {
    path: String,
    #[serde(default = "default_float")]
    float: bool,
    #[serde(default = "default_tiles")]
    tiles: bool,
    channels: usize,
}

fn default_float() -> bool {
    false
}

fn default_tiles() -> bool {
    true
}

type TexturesToml = HashMap<String, TextureToml>;

#[derive(Debug)]
pub struct Textures {
    textures: HashMap<String, Texture>,

    #[cfg(debug)]
    toml: TexturesToml,
}

impl Textures {
    pub fn open<P: AsRef<Path>>(_: &Renderer, path: P) -> Result<Self> {
        let mut file = File::open(path.as_ref())?;
        let mut contents = String::new();

        file.read_to_string(&mut contents)?;

        let toml_defs: TexturesToml = from_str(contents.as_str()).unwrap();
        Self::from_toml(&toml_defs)
    }

    fn from_toml(toml: &TexturesToml) -> Result<Self> {
        let mut textures = HashMap::new();

        for (name, def) in toml.clone() {
            if def.float {
                let image = Image::from_reader(File::open(&def.path)?, def.channels)
                    .map_err(|_| Error::Other("Error opening image file"))?;

                let (internal, format) = match def.channels {
                    1 => Ok((gl::R32F, gl::RED)),
                    2 => Ok((gl::RG32F, gl::RG)),
                    3 => Ok((gl::RGB32F, gl::RGB)),
                    4 => Ok((gl::RGBA32F, gl::RGBA)),
                    _ => Err(Error::Other("Invalid number of channels")),
                }?;

                textures.insert(
                    name,
                    create_texture(
                        image.as_ptr() as *const f32 as _,
                        image.width(),
                        image.height(),
                        internal,
                        format,
                        gl::FLOAT as _,
                        if def.tiles {
                            gl::REPEAT
                        } else {
                            gl::CLAMP_TO_EDGE
                        } as _,
                    )?,
                );
            } else {
                let image = Image::from_reader(File::open(&def.path)?, def.channels)
                    .map_err(|_| Error::Other("Error opening image file"))?;

                let (internal, format) = match def.channels {
                    1 => Ok((gl::R8, gl::RED)),
                    2 => Ok((gl::RG8, gl::RG)),
                    3 => Ok((gl::RGB8, gl::RGB)),
                    4 => Ok((gl::RGBA8, gl::RGBA)),
                    _ => Err(Error::Other("Invalid number of channels")),
                }?;

                textures.insert(
                    name,
                    create_texture(
                        image.as_ptr() as *const u8 as _,
                        image.width(),
                        image.height(),
                        internal,
                        format,
                        gl::UNSIGNED_BYTE as _,
                        if def.tiles {
                            gl::REPEAT
                        } else {
                            gl::CLAMP_TO_EDGE
                        } as _,
                    )?,
                );
            }
        }

        Ok(Textures {
            textures,

            #[cfg(debug)]
            toml: toml.clone(),
        })
    }

    #[cfg(debug)]
    pub fn reload_all(&mut self) -> Result<()> {
        let Self { textures, .. } = Self::from_toml(&self.toml)?;

        self.textures = textures;
        Ok(())
    }

    pub fn get(&self, name: &str) -> Option<&Texture> {
        self.textures.get(name)
    }
}

pub fn create_texture(
    data: *const (),
    width: usize,
    height: usize,
    internal: GLenum,
    format: GLenum,
    type_: GLenum,
    wrap: GLenum,
) -> Result<Texture> {
    unsafe {
        let mut id = 0;
        check!(gl::GenTextures(1, &mut id));

        check!(gl::ActiveTexture(gl::TEXTURE0));
        check!(gl::BindTexture(gl::TEXTURE_2D, id));
        check!(gl::TexParameteri(
            gl::TEXTURE_2D,
            gl::TEXTURE_MAG_FILTER,
            gl::LINEAR as _
        ));
        check!(gl::TexParameteri(
            gl::TEXTURE_2D,
            gl::TEXTURE_MIN_FILTER,
            gl::LINEAR as _
        ));
        check!(gl::TexParameteri(
            gl::TEXTURE_2D,
            gl::TEXTURE_WRAP_S,
            wrap as _
        ));
        check!(gl::TexParameteri(
            gl::TEXTURE_2D,
            gl::TEXTURE_WRAP_T,
            wrap as _
        ));

        check!(gl::TexImage2D(
            gl::TEXTURE_2D,
            0,
            internal as _,
            width as _,
            height as _,
            0,
            format,
            type_ as _,
            data as _,
        ));

        check!(gl::BindTexture(gl::TEXTURE_2D, 0));

        Ok(Texture { handle: id })
    }
}
